import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';



@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
  
  
})
export class ContactComponent implements OnInit {

   registerForm: FormGroup;

    constructor(private formBuilder: FormBuilder) { }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            tel: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
			message: ['', Validators.required],
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    onSubmit() {
        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }
		
  

        alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value));
		
		this.registerForm.reset({
        firstName: '',
            lastName: '',
            tel: '',
            email: '',
			message: ''
    });
    }

}
