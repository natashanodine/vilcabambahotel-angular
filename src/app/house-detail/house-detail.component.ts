import { Location } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Params, ActivatedRoute } from '@angular/router';


import { Comment } from '../shared/comment';
import { House } from '../shared/house';
import { HouseService } from '../services/house.service';

@Component({
  selector: 'app-house-detail',
  templateUrl: './house-detail.component.html',
  styleUrls: ['./house-detail.component.css']
})
export class HouseDetailComponent implements OnInit {

   house: House;
   houses: House[];
  comment: Comment;
  commentForm: FormGroup;
  errMess: string;
  
  
  formErrors = {
    'author' : '',
    'rating' : '',
    'comment' : ''
  };

  validationMessages = {
    'author' : {
      'required' : 'Name is required',
      'minlength' : 'Name must be at least 2 characters long',
      'maxlength' : 'Name cannot be more that 25 characters long'
    }
  };

 
  constructor(
    private houseService: HouseService,
	 private fb: FormBuilder,
    private location: Location,
    private route: ActivatedRoute,
    @Inject("BaseURL") private BaseURL
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.getHouse();
	this.getHouses();
		
	
  }
 
  getHouse(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.houseService.getHouse(id)
      .subscribe(house => this.house = house);
  }
 
 
  getHouses(): void {
    this.houseService.getHouses()
    .subscribe(houses => this.houses = houses);
  }
 
    createForm() {
    this.commentForm = this.fb.group({
      author: ['', [ Validators.required, Validators.minLength(2) ] ],
      rating: 5,
      comment: ['', [ Validators.required ] ],
    });

    this.commentForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set form validation messages
  }
  
    onValueChanged(commentFormData?: any) {
    if (!this.commentForm) {
      return;
    }
    const form = this.commentForm;
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }

    if (this.commentForm.valid) {
      this.comment = this.commentForm.value;
    } else {
      this.comment = undefined;
    }
  }

  onSubmit() {
	  const id = +this.route.snapshot.paramMap.get('id');
	      this.comment['date'] = new Date().toISOString();

    this.house.comments.push(this.comment);
    this.houseService.updatePosts(this.house.id, this.comment).subscribe(() => {
    console.log("PUT is done");
})
	  
    this.commentForm.reset({
        author: '',
        rating: 5,
        comment: ''
    });
  }
  
  
}