import { Component, OnInit } from '@angular/core';

import { Comment } from '../shared/comment';
import { Cabin } from '../shared/cabin';

import { CabinService } from '../services/cabin.service';

@Component({
  selector: 'app-cabins',
  templateUrl: './cabins.component.html',
  styleUrls: ['./cabins.component.css']
})
export class CabinsComponent implements OnInit {

  cabins: Cabin[];
  
  constructor(private cabinService: CabinService) { }
 
  ngOnInit() {
    this.getCabins();
  }
 
  getCabins(): void {
    this.cabinService.getCabins()
    .subscribe(cabins => this.cabins = cabins);
  }
 
}
