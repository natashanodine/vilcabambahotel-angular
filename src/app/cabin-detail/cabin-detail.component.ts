import { Location } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Params, ActivatedRoute } from '@angular/router';


import { Comment } from '../shared/comment';
import { Cabin } from '../shared/cabin';
import { CabinService } from '../services/cabin.service';

@Component({
  selector: 'app-cabin-detail',
  templateUrl: './cabin-detail.component.html',
  styleUrls: ['./cabin-detail.component.css']
})
export class CabinDetailComponent implements OnInit {
   cabin: Cabin;
   cabins: Cabin[];
  comment: Comment;
  commentForm: FormGroup;
  errMess: string;
  
  
  formErrors = {
    'author' : '',
    'rating' : '',
    'comment' : ''
  };

  validationMessages = {
    'author' : {
      'required' : 'Name is required',
      'minlength' : 'Name must be at least 2 characters long',
      'maxlength' : 'Name cannot be more that 25 characters long'
    }
  };

 
  constructor(
    private cabinService: CabinService,
	 private fb: FormBuilder,
    private location: Location,
    private route: ActivatedRoute,
    @Inject("BaseURL") private BaseURL
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.getCabin();
	this.getCabins();
	
	
  }
 
  getCabin(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.cabinService.getCabin(id)
      .subscribe(cabin => this.cabin = cabin);
  }
 
 
  getCabins(): void {
    this.cabinService.getCabins()
    .subscribe(cabins => this.cabins = cabins);
  }
 
/* addComment(description: string): void {
    description = description.trim();
    if (!description) { return; }
    this.cabinService.addCabin({ description } as Cabin)
      .subscribe(cabin => {
        this.cabins.push(cabin);
      });
  }
 */
 /* delete(cabin: Cabin): void {
    this.cabins = this.cabins.filter(h => h !== cabin);
    this.cabinService.deleteCabin(cabin).subscribe();
  }
  */
  
    createForm() {
    this.commentForm = this.fb.group({
      author: ['', [ Validators.required, Validators.minLength(2) ] ],
      rating: 5,
      comment: ['', [ Validators.required ] ],
    });

    this.commentForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set form validation messages
  }
  
    onValueChanged(commentFormData?: any) {
    if (!this.commentForm) {
      return;
    }
    const form = this.commentForm;
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }

    if (this.commentForm.valid) {
      this.comment = this.commentForm.value;
    } else {
      this.comment = undefined;
    }
  }

  onSubmit() {
	  const id = +this.route.snapshot.paramMap.get('id');
	      this.comment['date'] = new Date().toISOString();

    this.cabin.comments.push(this.comment);
    this.cabinService.updatePosts(this.cabin.id, this.comment).subscribe(() => {
    console.log("PUT is done");
})
	  
    this.commentForm.reset({
        author: '',
        rating: 5,
        comment: ''
    });
  }
  
  
}
