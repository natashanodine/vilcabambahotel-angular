import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CabinDetailComponent } from './cabin-detail.component';

describe('CabinDetailComponent', () => {
  let component: CabinDetailComponent;
  let fixture: ComponentFixture<CabinDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CabinDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CabinDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
