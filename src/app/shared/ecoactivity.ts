export class Ecoactivity {
  id: number;
  name: string;
  description: string;
  image:  string;
  featured: boolean;
}