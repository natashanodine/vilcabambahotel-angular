import { Ecoactivity } from './ecoactivity';
import { User } from './user';

export class Favorite {
    id: number;
    user: User;
    ecoactivities: Ecoactivity[];
    createdAt: string;
    updatedAt: string;
}
