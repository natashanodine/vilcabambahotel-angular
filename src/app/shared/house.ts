import { Comment } from './comment';

export class House {
  id: number;
  name: string;
  description: string;
  pricemonth:  string;
  image:  string;
  featured: boolean;
  comments: Comment[];
}