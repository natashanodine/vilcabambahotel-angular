export class User {
    id: number;
    username: string;
    firstname: string;
    lastname: string;
    admin: boolean;
}
