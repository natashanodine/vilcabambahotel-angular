import { Comment } from './comment';

export class Cabin {
  id: string;
  name: string;  
  image:  string;
  description: string;
  priceweek:  string;
  pricemonth:  string;
  featured: boolean;
  comments: Comment[];
}