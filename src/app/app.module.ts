import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


import { HttpClientModule }    from '@angular/common/http';




import { AppComponent } from './app.component';
import { CabinsComponent } from './cabins/cabins.component';
import { HousesComponent } from './houses/houses.component';
import { EcoactivitiesComponent } from './ecoactivities/ecoactivities.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MessagesComponent } from './messages/messages.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { CabinDetailComponent } from './cabin-detail/cabin-detail.component';
import { HouseDetailComponent } from './house-detail/house-detail.component';
import { HomecarouselComponent } from './homecarousel/homecarousel.component';
import { FootermodalComponent } from './footermodal/footermodal.component';


import { CabinService } from './services/cabin.service';
import { EcoactivityService } from './services/ecoactivity.service';
import { HouseService } from './services/house.service';
import {AuthService } from './services/auth.service';
import {AuthGuardService } from './services/auth-guard.service';
import {ProcessHTTPMsgService } from './services/process-httpmsg.service';
import { MessageService } from './services/message.service';
import { baseURL } from './shared/baseurl';


import { AuthInterceptor, UnauthorizedInterceptor } from './services/auth.interceptor';

import {HTTP_INTERCEPTORS} from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    CabinsComponent,
    HousesComponent,
    EcoactivitiesComponent,
    ContactComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    MessagesComponent,
    CabinDetailComponent,
    HouseDetailComponent,
    HomecarouselComponent,
    FootermodalComponent
  ],
  imports: [
     AppRoutingModule,
    BrowserModule,
	NgbModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
	HttpClientModule
	
  ],
  providers: [
  CabinService,
  EcoactivityService,
  HouseService,
  MessageService,
  AuthService,
  AuthGuardService,
  ProcessHTTPMsgService,
    { provide: 'BaseURL', useValue: baseURL },
   {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: UnauthorizedInterceptor,
      multi: true
    }
  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
