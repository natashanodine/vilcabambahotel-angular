import { Component, Inject, OnInit } from '@angular/core';

import { Cabin } from '../shared/cabin';
import { CabinService } from '../services/cabin.service';

import { House } from '../shared/house';
import { HouseService } from '../services/house.service';

import { Ecoactivity } from '../shared/ecoactivity';
import { EcoactivityService } from '../services/ecoactivity.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  cabins: Cabin[];
  houses: House[];
  ecoactivities: Ecoactivity[];
  cabinErrMess: string;

  constructor(
    private cabinService: CabinService,
    private houseService: HouseService,
    private ecoactivityService: EcoactivityService,
    @Inject('BaseURL') private BaseURL) {
    }
	
	
	
  ngOnInit() {
    this.getFeaturedCabin();
    this.getFeaturedHouse();
    this.getFeaturedEcoactivity();
  }
 
  getFeaturedCabin(): void {
    this.cabinService.getFeaturedCabin()
    .subscribe(cabins => this.cabins = cabins);
  }
 
  getFeaturedHouse(): void {
    this.houseService.getFeaturedHouse()
    .subscribe(houses => this.houses = houses);
  }
 
  getFeaturedEcoactivity(): void {
    this.ecoactivityService.getFeaturedEcoactivity()
    .subscribe(ecoactivities => this.ecoactivities = ecoactivities);
  }
 
 
}