import { Component, OnInit } from '@angular/core';

import { Ecoactivity } from '../shared/ecoactivity';
import { EcoactivityService } from '../services/ecoactivity.service';



@Component({
  selector: 'app-ecoactivities',
  templateUrl: './ecoactivities.component.html',
  styleUrls: ['./ecoactivities.component.css']
})
export class EcoactivitiesComponent implements OnInit {
  ecoactivities: Ecoactivity[];
  ecoactivity: Ecoactivity;
  
  favorite = false;
  
  constructor(private ecoactivityService: EcoactivityService) { }
 
  ngOnInit() {
    this.getEcoactivities();
  }
 
  getEcoactivities(): void {
    this.ecoactivityService.getEcoactivities()
    .subscribe(ecoactivities => this.ecoactivities = ecoactivities);
  }
}

