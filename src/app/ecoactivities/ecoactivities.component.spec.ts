import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EcoactivitiesComponent } from './ecoactivities.component';

describe('EcoactivitiesComponent', () => {
  let component: EcoactivitiesComponent;
  let fixture: ComponentFixture<EcoactivitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EcoactivitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcoactivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
