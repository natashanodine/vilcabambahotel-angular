import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-footermodal',
  templateUrl: './footermodal.component.html',
  styleUrls: ['./footermodal.component.css']
})
export class FootermodalComponent implements OnInit {
	
	
closeResult: string;
registerForm: FormGroup;

  constructor(private modalService: NgbModal,
  private formBuilder: FormBuilder) {}

   ngOnInit() {
        this.registerForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            tel: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
			message: ['', Validators.required],
        });
    }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  
   get f() { return this.registerForm.controls; }
   
    onSubmit() {
        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value));
		
		this.registerForm.reset({
        firstName: '',
            lastName: '',
            tel: '',
            email: '',
			message: ''
    });
    }

}