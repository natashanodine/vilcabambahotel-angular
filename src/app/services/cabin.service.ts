import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap, flatMap } from 'rxjs/operators';

import { Cabin } from '../shared/cabin';
import { Comment } from '../shared/comment';
import { MessageService } from './message.service';



const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class CabinService {

  private cabinsUrl = 'http://localhost:3000/cabins';  // URL to web api

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** GET cabins from the server */
  getCabins(): Observable<Cabin[]> {
    return this.http.get<Cabin[]>(this.cabinsUrl)
      .pipe(
        tap(cabins => this.log('fetched cabins')),
        catchError(this.handleError('getCabins', []))
      );
  }

  getFeaturedCabin(): Observable<Cabin[]> {
    const url = 'http://localhost:3000/cabins?featured=true';
    return this.http.get<Cabin[]>(url).pipe(
      tap(_ => this.log('o')),
      catchError(this.handleError<Cabin[]>(`getFeaturedCabin`))
    );
  }
  /** GET cabin by id. Return `undefined` when id not found */
  getCabinNo404<Data>(id: number): Observable<Cabin> {
    const url = `${this.cabinsUrl}/?id=${id}`;
    return this.http.get<Cabin[]>(url)
      .pipe(
        map(cabins => cabins[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} cabin id=${id}`);
        }),
        catchError(this.handleError<Cabin>(`getCabin id=${id}`))
      );
  }

  /** GET cabin by id. Will 404 if id not found */
  getCabin(id: number): Observable<Cabin> {
    const url = `${this.cabinsUrl}/${id}`;
    return this.http.get<Cabin>(url).pipe(
      tap(_ => this.log(`fetched cabin id=${id}`)),
      catchError(this.handleError<Cabin>(`getCabin id=${id}`))
    );
  }



updatePosts(id, newcomment) {
    const comment: Comment = newcomment;
    return this.http.get<Cabin>('http://localhost:3000/cabins/' + id).pipe(
      map(cabin => {


        return {
          id: cabin.id,
          name: cabin.name,
		  image: cabin.image,
          description: cabin.description,
          priceweek: cabin.priceweek,
          pricemonth: cabin.pricemonth,
          featured: cabin.featured,
          comments: cabin.comments


        };


      }),
      flatMap((updatedCabin) => {
        updatedCabin.comments.push(comment);
        return this.http.put(this.cabinsUrl + '/' + id, updatedCabin);
      })
    );

  }


  
  
  
   /**
    * Handle Http operation that failed.
    * Let the app continue.
    * @param operation - name of the operation that failed
    * @param result - optional value to return as the observable result
    */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a CabinService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`CabinService: ${message}`);
  }

}
