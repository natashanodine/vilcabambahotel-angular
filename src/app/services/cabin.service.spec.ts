import { TestBed } from '@angular/core/testing';

import { CabinService } from './cabin.service';

describe('CabinService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CabinService = TestBed.get(CabinService);
    expect(service).toBeTruthy();
  });
});
