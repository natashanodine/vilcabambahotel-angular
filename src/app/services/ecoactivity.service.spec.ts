import { TestBed } from '@angular/core/testing';

import { EcoactivityService } from './ecoactivity.service';

describe('EcoactivityService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EcoactivityService = TestBed.get(EcoactivityService);
    expect(service).toBeTruthy();
  });
});
