import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap, flatMap } from 'rxjs/operators';

import { House } from '../shared/house';
import { Comment } from '../shared/comment';
import { MessageService } from './message.service';



const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class HouseService {

  private housesUrl = 'http://localhost:3000/houses';  // URL to web api

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** GET houses from the server */
  getHouses(): Observable<House[]> {
    return this.http.get<House[]>(this.housesUrl)
      .pipe(
        tap(houses => this.log('fetched houses')),
        catchError(this.handleError('getHouses', []))
      );
  }

  getFeaturedHouse(): Observable<House[]> {
    const url = 'http://localhost:3000/houses?featured=true';
    return this.http.get<House[]>(url).pipe(
      tap(_ => this.log('o')),
      catchError(this.handleError<House[]>(`getFeaturedHouse`))
    );
  }
  /** GET house by id. Return `undefined` when id not found */
  getHouseNo404<Data>(id: number): Observable<House> {
    const url = `${this.housesUrl}/?id=${id}`;
    return this.http.get<House[]>(url)
      .pipe(
        map(houses => houses[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} house id=${id}`);
        }),
        catchError(this.handleError<House>(`getHouse id=${id}`))
      );
  }

  /** GET house by id. Will 404 if id not found */
  getHouse(id: number): Observable<House> {
    const url = `${this.housesUrl}/${id}`;
    return this.http.get<House>(url).pipe(
      tap(_ => this.log(`fetched house id=${id}`)),
      catchError(this.handleError<House>(`getHouse id=${id}`))
    );
  }



updatePosts(id, newcomment) {
    const comment: Comment = newcomment;
    return this.http.get<House>('http://localhost:3000/houses/' + id).pipe(
      map(house => {


        return {
          id: house.id,
          name: house.name,
		  image: house.image,
          description: house.description,
          pricemonth: house.pricemonth,
          featured: house.featured,
          comments: house.comments


        };


      }),
      flatMap((updatedHouse) => {
        updatedHouse.comments.push(comment);
        return this.http.put(this.housesUrl + '/' + id, updatedHouse);
      })
    );

  }


  
  

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HouseService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`HouseService: ${message}`);
  }

}
