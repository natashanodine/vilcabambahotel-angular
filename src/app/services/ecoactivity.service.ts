import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap, flatMap } from 'rxjs/operators';

import { Ecoactivity } from '../shared/ecoactivity';
import { Comment } from '../shared/comment';
import { MessageService } from './message.service';



const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class EcoactivityService {

  private ecoactivitiesUrl = 'http://localhost:3000/ecoactivities';  // URL to web api

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** GET ecoactivities from the server */
  getEcoactivities(): Observable<Ecoactivity[]> {
    return this.http.get<Ecoactivity[]>(this.ecoactivitiesUrl)
      .pipe(
        tap(ecoactivities => this.log('fetched ecoactivities')),
        catchError(this.handleError('getEcoactivities', []))
      );
  }

  getFeaturedEcoactivity(): Observable<Ecoactivity[]> {
    const url = 'http://localhost:3000/ecoactivities?featured=true';
    return this.http.get<Ecoactivity[]>(url).pipe(
      tap(_ => this.log('o')),
      catchError(this.handleError<Ecoactivity[]>(`getFeaturedEcoactivity`))
    );
  }
  /** GET ecoactivity by id. Return `undefined` when id not found */
  getEcoactivityNo404<Data>(id: number): Observable<Ecoactivity> {
    const url = `${this.ecoactivitiesUrl}/?id=${id}`;
    return this.http.get<Ecoactivity[]>(url)
      .pipe(
        map(ecoactivities => ecoactivities[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} ecoactivity id=${id}`);
        }),
        catchError(this.handleError<Ecoactivity>(`getEcoactivity id=${id}`))
      );
  }

  /** GET ecoactivity by id. Will 404 if id not found */
  getEcoactivity(id: number): Observable<Ecoactivity> {
    const url = `${this.ecoactivitiesUrl}/${id}`;
    return this.http.get<Ecoactivity>(url).pipe(
      tap(_ => this.log(`fetched ecoactivity id=${id}`)),
      catchError(this.handleError<Ecoactivity>(`getEcoactivity id=${id}`))
    );
  }


  
   /**
    * Handle Http operation that failed.
    * Let the app continue.
    * @param operation - name of the operation that failed
    * @param result - optional value to return as the observable result
    */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a EcoactivityService message with the MessageService */
  private log(message: string) {
    this.messageService.add(`EcoactivityService: ${message}`);
  }

}
