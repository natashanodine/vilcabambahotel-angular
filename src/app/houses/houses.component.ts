import { Component, OnInit } from '@angular/core';

import { Comment } from '../shared/comment';
import { House } from '../shared/house';
import { HouseService } from '../services/house.service';

@Component({
  selector: 'app-houses',
  templateUrl: './houses.component.html',
  styleUrls: ['./houses.component.css']
})
export class HousesComponent implements OnInit {


  houses: House[];
  
  constructor(private houseService: HouseService) { }
 
  ngOnInit() {
    this.getHouses();
  }
 
  getHouses(): void {
    this.houseService.getHouses()
    .subscribe(houses => this.houses = houses);
  }
 
}
